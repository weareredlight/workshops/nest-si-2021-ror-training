- [Start the application](#start-the-application)
  - [Start the containers](#start-the-containers)
  - [Stop the containers](#stop-the-containers)
  - [Force a rebuild](#force-a-rebuild)
  - [Restart the app](#restart-the-app)
- [Commands](#commands)
  - [Run migrations](#run-migrations)
  - [Open console](#open-console)
  - [Generate a new Model](#generate-a-new-model)
  - [Generate a new Controller](#generate-a-new-controller)
  - [Generate Model, Controller and Views with one command](#generate-model-controller-and-views-with-one-command)
  - [Routes](#routes)
  - [Available tasks](#available-tasks)
- [Create a new App using docker](#create-a-new-app-using-docker)


# Start the application
## Start the containers
```
docker-compose up -d
```

## Stop the containers
```
docker-compose stop
```

## Force a rebuild
```
docker-compose up --force-recreate --build -d
```

## Restart the app
```
docker-compose run --no-deps --rm rails bundle exec rails restart
```

# Commands
```
docker-compose run --no-deps --rm <command>
```

## Run migrations
```
docker-compose run --no-deps --rm rails bundle exec rails db:migrate
```

## Open console
```
docker-compose run --no-deps --rm rails bundle exec rails console
```

## Generate a new Model
The model name should be in the singular form, example: `post`
```
docker-compose run --no-deps --rm rails bundle exec rails generate model <model_name>
```

## Generate a new Controller
The controller name should be in the plural form, example: `posts`
```
docker-compose run --no-deps --rm rails bundle exec rails generate controller <controller_name>
```

## Generate Model, Controller and Views with one command
The name should be in the singular form, example: `post`
```
docker-compose run --no-deps --rm rails bundle exec rails generate scaffold <name>
```

## Routes
```
docker-compose run --no-deps --rm rails bundle exec rails routes
```

## Available tasks
```
docker-compose run --no-deps --rm rails bundle exec rails -T
```

# Create a new App using docker
To create a new app using docker you should use the docker config inside the `starter-docker` folder and run the following command after starting the containers:
```
docker-compose run --no-deps --rm rails bundle exec rails new . --force --database=postgresql
```
