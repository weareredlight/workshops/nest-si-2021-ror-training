class Post < ApplicationRecord
  belongs_to :user
  has_many :comments

  has_one_attached :media

  validates_presence_of :description
end
