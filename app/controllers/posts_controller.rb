class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :update, :edit, :destroy]

  def index
    @posts = Post.order(created_at: :desc).all

    respond_to do |format|
      format.html { render :index }
      format.json { render json: @posts }
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      redirect_to posts_path
    else
      render :new
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])

    if @post.user_id == current_user.id
      if @post.update(post_params)
        redirect_to posts_path
      else
        render :update
      end
    else
      redirect_to posts_path, notice: "You don't have permissions"
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to posts_path, notice: "Post deleted"
  end

  protected

=begin
{
  post: {
    description: STRING,
    media: FILE
  }
}
=end
  def post_params
    params.require(:post).permit(:description, :media)
  end
end
