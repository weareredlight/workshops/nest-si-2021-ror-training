class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:create]

  def create
    post = Post.find(params[:post_id])
    comment = post.comments.new(comment_params)
    comment.user = current_user

    if comment.save
      redirect_to post_path(post)
    else
      redirect_to post_path(post)
    end
  end

  protected

  def comment_params
    params.require(:comment).permit(:text)
  end
end
